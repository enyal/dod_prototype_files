#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main()
{
	ofstream outputFile;
	outputFile.open("MODELDEF.txt");
	string modelName;
	string modelFile;
	string pathName;
	string spriteName;
	int maxFrame = 0;
	
	cout << "enter the base model name (ex: Gun): ";
	cin >> modelName;
	cout << "enter the model name and the extension (ex: v_gun.md3): ";
	cin >> modelFile;
	cout << "enter the path name (ex: models/: ";
	cin >> pathName;
	cout << "enter the sprite name (only the first 2 letters or it wont work, ex: AB): ";
	cin >> spriteName;
	cout << "enter the max amount of sprites in the animation file (higher than 0, only integers): ";
	cin >> maxFrame;
	
	outputFile << "model " << modelName << endl << "{" << endl << "Path " << "\"" << pathName << "\"" << endl << "Model 0 " << "\"" << modelFile << "\"" << endl << "Scale -1 1 1" << endl << "Offset 0 0 0" << endl;
	int p = 0;
	maxFrame++;
			for(char i = 'A'; i!= '['; i++)
			{
				for(int j = 1; j<10; j++)
				{
					for(char k = 'A'; k!= '['; k++)
					{
						if(p==maxFrame)
						{
							break;
						}
						outputFile << "FrameIndex " << spriteName << i << j << " " << k << " 0 " << p << endl;
						p++;
					
					}	
				}
			}
	
	
	outputFile << "}" << endl;
	outputFile.close();
	cout << "finished\n";
	return 0;
}

